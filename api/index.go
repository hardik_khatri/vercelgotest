package main

import (
    "encoding/json"
    "math/rand"
    "net/http"
    "time"

    "github.com/gorilla/mux"
)

type Animal struct {
    Name  string `json:"name"`
    Sound string `json:"sound"`
}

var animals = []Animal{
    {"Dog", "Woof"},
    {"Cat", "Meow"},
    {"Cow", "Moo"},
    // Add more animals here
}

func getRandomAnimal() Animal {
    rand.Seed(time.Now().UnixNano())
    return animals[rand.Intn(len(animals))]
}

func GetRandomAnimal(w http.ResponseWriter, r *http.Request) {
    animal := getRandomAnimal()
    json.NewEncoder(w).Encode(animal)
}

func main() {
    r := mux.NewRouter()
    r.HandleFunc("/random-animal", GetRandomAnimal).Methods("GET")

    http.Handle("/", r)

    http.ListenAndServe(":8080", nil)
}
